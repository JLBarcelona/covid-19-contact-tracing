<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'barangay', 'title' => 'Barangay', 'icon' => asset('img/logo.png') ])
<body class="sidebar-mini layout-fixed" onload="show_disapuser();">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'barangay'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'barangay'])
    <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 mt-3">
              <div class="card">
                <div class="card-header h4">
                  <i class="fa fa-house-user nav-icon"></i> Disapproved
                </div>
                <div class="card-body">
                  <table class="table table-bordered dt-responsive nowrap" id="tbl_disapuser" style="width: 100%;"></table>
                </div>
                <div class="card-footer"></div>
              </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'barangay'])
</html>

<script>
  

</script>