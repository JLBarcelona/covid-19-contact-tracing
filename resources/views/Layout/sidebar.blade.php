@php
  $parent = '';
  $child = '';
  $active = url()->current();
@endphp

@if($type == 'home')

@elseif($type == 'user')
 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{ asset('img/logo.png') }}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3 bg-white">
           <span class="brand-text font-weight-light">User</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          @if(!empty(Auth::user()->profile_picture))
            <img src="{{ asset('profile_picture/'.Auth::user()->profile_picture) }}" class="img-circle elevation-2" alt="User Image">
          @else
            <img src="{{ asset('img/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
          @endif
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->fullname }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview {{ (request()->is('report/salary') || request()->is('report/attendance')) ? 'menu-open' : '' }} ">
            <a href="#" class="nav-link {{ (request()->is('report/salary') || request()->is('report/attendance')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Report
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('report/attendance') }}" class="nav-link {{ (request()->is('report/attendance')) ? 'active' : '' }}">
                  <i class="nav-icon fas fa-calendar"></i>
                  <p>Attendance</p>
                </a>
              </li>
              <li class="nav-item active">
                <a href="{{ url('report/salary') }}" class="nav-link {{ (request()->is('report/salary')) ? 'active' : '' }}">
                  <i class="nav-icon fas fa-money-check-alt"></i>
                  <p>Salary</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item active">
            <a href="{{ url('profile') }}" class="nav-link {{ (request()->is('profile')) ? 'active' : '' }}">
              <i class="fa fa-user nav-icon"></i>
              <p>Profile</p>
            </a>
          </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
@elseif($type == 'admin')
 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{ asset('img/logo.png') }}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3 bg-white">
           <span class="brand-text font-weight-light">Admin</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          @if(!empty(Auth::user()->profile_picture))
            <img src="{{ asset('profile_picture/'.Auth::user()->profile_picture) }}" class="img-circle elevation-2" alt="User Image">
          @else
            <img src="{{ asset('img/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
          @endif
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->owner->admin_name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ url('admin') }}" class="nav-link {{ (request()->is('admin')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview {{ (request()->is('admin/barangay') || request()->is('admin/store') || request()->is('admin/profile')) ? 'menu-open' : '' }} ">
            <a href="#" class="nav-link {{ (request()->is('admin/barangay') || request()->is('admin/store') || request()->is('admin/profile')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                User Account
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('admin/barangay') }}" class="nav-link {{ (request()->is('admin/barangay')) ? 'active' : '' }}">
                  <i class="fa fa-house-user nav-icon"></i>
                  <p>Barangay</p>
                </a>
              </li>
              <li class="nav-item active">
                <a href="{{ url('admin/store') }}" class="nav-link {{ (request()->is('admin/store')) ? 'active' : '' }}">
                  <i class="fa fa-store-alt nav-icon"></i>
                  <p>Store</p>
                </a>
              </li>
              <li class="nav-item active">
                <a href="{{ url('admin/profile') }}" class="nav-link {{ (request()->is('admin/profile')) ? 'active' : '' }}">
                  <i class="fa fa-user nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/report') }}" class="nav-link {{ (request()->is('admin/report')) ? 'active' : '' }}">
              <i class="fa fa-file-alt nav-icon"></i>
              <p>
                Report
              </p>
            </a>
          </li>
          <!-- <li class="nav-header">EXAMPLES</li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  @elseif($type == 'barangay')
 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{ asset('img/logo.png') }}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3 bg-white">
           <span class="brand-text font-weight-light">BRGY. {{ Auth::user()->owner->barangay }}</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          @if(!empty(Auth::user()->profile_picture))
            <img src="{{ asset('profile_picture/'.Auth::user()->profile_picture) }}" class="img-circle elevation-2" alt="User Image">
          @else
            <img src="{{ asset('img/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
          @endif
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->owner->barangay_representative }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ url('barangay') }}" class="nav-link {{ (request()->is('barangay')) ? 'active' : '' }}">
              <i class="fa fa-house-user nav-icon"></i>
              <p>
                Barangay
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview {{ (request()->is('barangay/user/pending') || request()->is('barangay/user/approved') || request()->is('barangay/user/disapproved')) ? 'menu-open' : '' }} ">
            <a href="#" class="nav-link {{ (request()->is('barangay/user/pending')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Profile
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('barangay/user/pending') }}" class="nav-link {{ (request()->is('barangay/user/pending')) ? 'active' : '' }}">
                  <i class="fa fa-user-clock nav-icon"></i>
                  <p>Pending</p>
                </a>
              </li>
              <li class="nav-item active">
                <a href="{{ url('barangay/user/approved') }}" class="nav-link {{ (request()->is('barangay/user/approved')) ? 'active' : '' }}">
                  <i class="fa fa-thumbs-up nav-icon"></i>
                  <p>Approved</p>
                </a>
              </li>
              <li class="nav-item active">
                <a href="{{ url('barangay/user/disapproved') }}" class="nav-link {{ (request()->is('barangay/user/disapproved')) ? 'active' : '' }}">
                  <i class="fa fa-thumbs-down nav-icon"></i>
                  <p>Disapproved</p>
                </a>
              </li>
            </ul>
          </li>
          <!-- <li class="nav-header">EXAMPLES</li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
@elseif($type == 'store')
     <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="../../index3.html" class="brand-link">
          <img src="{{ asset('img/logo.png') }}"
               alt="AdminLTE Logo"
               class="brand-image img-circle elevation-3 bg-white">
               <span class="brand-text font-weight-light">Store</span>
        </a>
        <!-- Sidebar -->
        <div class="sidebar" >
          <!-- Sidebar user (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              @if(!empty(Auth::user()->profile_picture))
                <img src="{{ asset('profile_picture/'.Auth::user()->profile_picture) }}" class="img-circle elevation-2" alt="User Image">
              @else
                <img src="{{ asset('img/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
              @endif
            </div>
            <div class="info">
              <a href="#" class="d-block">{{ Auth::user()->owner->store_name }}</a>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <li class="nav-item">
                <a href="{{ url('store') }}" class="nav-link {{ (request()->is('store')) ? 'active' : '' }}">
                  <i class="nav-icon fas fa-history"></i>
                  <p>
                    History
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('scanner') }}" target="_blank" class="nav-link {{ (request()->is('scanner')) ? 'active' : '' }}">
                  <i class="nav-icon fas fa-qrcode"></i>
                  <p>
                    Scanner
                  </p>
                </a>
              </li>
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>
@endif
