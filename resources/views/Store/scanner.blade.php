<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'store', 'title' => 'Store', 'icon' => asset('img/logo.png') ])
   <body class="" >
     <div class="wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
          <div class="col-sm-12 mt-5">
            <div class="card">
              <div class="card-header">
                <div class="media">
                  <img class="mr-3" src="{{ Auth::user()->owner->store_logo }}" alt="Generic placeholder image" width="90">
                  <div class="media-body">
                    <h1 class="mt-0">{{ Auth::user()->owner->store_name }}</h1>
                    {{ Auth::user()->owner->store_address }}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-3 form-group">
                    <img src="{{ asset('img/avatar.png') }}" alt="" class="img-thumbnail img-fluid" id="avatar" width="100%">
                     <form class="needs-validation" id="scan" action="{{ url('scan/add_scan') }}" novalidate>
                        <input type="password" id="user_code" name="user_code" placeholder="" autocomplete="off" autofocus="" class="form-control rounded-0" required>
                        <div class="invalid-feedback" id="err_user_code"></div>
                      </form>
                  </div>
                  <div class="col-sm-6 mt-2">
                    <!-- Name -->
                    <div class="row">
                      <div class="col-sm-3 h4">Name</div>
                      <div class="col-sm-1 h3">:</div>
                      <div class="col-sm-8 h3" id="name">N/A</div>
                    </div>
                    <!-- Brgy -->
                    <div class="row">
                      <div class="col-sm-3 h4">Barangay</div>
                      <div class="col-sm-1 h3">:</div>
                      <div class="col-sm-8 h3" id="barangay">N/A</div>
                    </div>
                    <!-- CP -->
                    <div class="row">
                      <div class="col-sm-3 h4">CP#</div>
                      <div class="col-sm-1 h3">:</div>
                      <div class="col-sm-8 h3" id="cp">N/A</div>
                    </div>
                    <!-- Temperature -->
                    <form class="needs-validation" id="logs_form" action="{{ url('report/add_report') }}" novalidate>
                      <div class="row">
                        <div class="col-sm-3 h4">Temperature</div>
                        <div class="col-sm-1 h3">:</div>
                        <div class="col-sm-8 h3">
                          <div class="row">
                            <div class="col-sm-6">
                              <input type="number" class="form-control" id="temperature" name="temperature" disabled="">
                              <div class="invalid-feedback" style="font-size: 10px !important;" id="err_temperature"></div>
                            </div>
                            <div class="col-sm-3 mr-0">
                              <div class="btn-group animated btn-process hide" role="group" aria-label="Basic example">
                                <button class="btn btn-success" type="submit">Proceed</button>
                                <button type="button" class="btn btn-dark " type="button" onclick="cancel_scan();">Cancel</button>
                              </div>
                            </div>
                          </div>
                          <input type="hidden" name="slug_id" id="slug_id">
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="col-sm-3"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
   </body>

   <audio id="success">
    <source src="{{ asset('sound/success.mp3') }}" type="audio/mpeg">
  </audio>

  <audio id="error">
    <source src="{{ asset('sound/error.mp3') }}" type="audio/mpeg">
  </audio>

  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'store'])
</html>

<script type="text/javascript">

  function cancel_scan(){
    $("#name").text('N/A');
    $("#barangay").text('N/A');
    $("#cp").text('N/A');
    $("#slug_id").val('');
    $("#avatar").attr('src', url_path('/img/avatar.png'));
    $("#temperature").attr('disabled', true);
    $("#user_code").val('');
    $("#user_code").focus();
    $("#temperature").val('');
    $(".btn-process").addClass('hide');
    $(".btn-process").removeClass('bounceIn');
  }

  $("#logs_form").on('submit', function(e){
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);
    var temperature = $("#temperature");

    if (temperature.val() >= 37 && temperature.val() <= 47) {
        temperature.focus();
        swal("Body Temperature is high","Your temperature is "+temperature.val()+" !\n\n Please try again.", "error");
        temperature.val('');
    }else if(temperature.val() >= 47){
        temperature.focus();
        temperature.val('');
    }else{
       $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
            //<!-- your before success function -->
        },
        success:function(response){
            console.log(response)
          if(response.status == true){
            sound = document.getElementById("success");
            // console.log(response)
            $("#name").text('N/A');
            $("#barangay").text('N/A');
            $("#cp").text('N/A');
            $("#slug_id").val('');
            $("#avatar").attr('src', url_path('/img/avatar.png'));
            $("#temperature").attr('disabled', true);
            $("#user_code").val('');
            $("#user_code").focus();
            $("#temperature").val('');
            $(".btn-process").addClass('hide');
            $(".btn-process").removeClass('bounceIn');

            swal("Success", response.message, "success");
            showValidator(response.error,'logs_form');
          }else{
            //<!-- your error message or action here! -->
            showValidator(response.error,'logs_form');
          }

          playAudio();

        },
        error:function(error){
          console.log(error)
        }
      });
    }
  });

</script>
<script type="text/javascript">
  var sound; 

  $("#scan").on('submit', function(e){
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          console.log(response);
        if(response.status == true){
          sound = document.getElementById("success");
          console.log(response);
          $("#name").text(response.data.fullname);
          $("#barangay").text(response.data.baranggay);
          $("#cp").text(response.data.contact_number);
          $("#slug_id").val(response.data.slug_id);
          $("#avatar").attr('src', response.data.avatar);
          $("#temperature").attr('disabled', false);
          $("#temperature").focus();
          $("#user_code").val('');

          $(".btn-process").removeClass('hide');
          $(".btn-process").addClass('bounceIn');

          // swal("Success", response.message, "success");
          showValidator(response.error,'scan');
        }else{
          sound = document.getElementById("error");
          swal("Error", response.message, "error");
          $("#user_code").val('');
          //<!-- your error message or action here! -->
          showValidator(response.error,'scan');
        }

        playAudio();

      },
      error:function(error){
        console.log(error)
      }
    });
  });



  function playAudio() { 
    sound.play(); 
  } 
</script>