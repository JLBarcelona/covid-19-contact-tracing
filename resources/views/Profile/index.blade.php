<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'user', 'title' => 'Profile', 'icon' => asset('img/logo.png') ])
<body class="sidebar-mini layout-fixed" onload="show_profile();">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'user'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'user'])
    <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-sm-12 mt-3">
                <div class="card">
                    <div class="card-header h4"><i class="fa fa-user"></i> <span>Users Profile</span>
						<button class="btn btn-primary btn-sm float-right" onclick="form_reset(); add_profile()"><i class="fa fa-plus"></i> Add User</button>
               		</div>
					<div class="card-body">
					<table class="table table-bordered dt-responsive nowrap" id="tbl_profile" style="width: 100%;"></table>
				    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
            </div>
        </div>
      </section>
    </div>
  </div>
</body>

 <div class="modal fade" role="dialog" id="modal_add_profile">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Add User
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
		  <form class="needs-validation" id="profile_form_id" action="{{ url('profile/add_profile') }}" novalidate>
				<div class="form-row">
					<input type="hidden" id="user_id" name="user_id" placeholder="" class="form-control" required>
					<div class="form-group col-sm-">
						<label>Slug Id </label>
						<input type="text" id="slug_id" name="slug_id" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_slug_id"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Qrcode Path </label>
						<input type="text" id="qrcode_path" name="qrcode_path" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_qrcode_path"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Firstname </label>
						<input type="text" id="firstname" name="firstname" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_firstname"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Middlename </label>
						<input type="text" id="middlename" name="middlename" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_middlename"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Lastname </label>
						<input type="text" id="lastname" name="lastname" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_lastname"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Birthdate </label>
						<input type="date" id="birthdate" name="birthdate" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_birthdate"></div>
					</div>
					<label>Gender </label>
						<div class="form-group col-sm-">
						<select id="gender" name="gender" class="form-control ">
							<option value="Male">Male</option>
							<option value="Female">Female</option>	
						</select>
						<div class="invalid-feedback" id="err_gender"></div>
					</div>
						<div class="form-group col-sm-">
						<label>Municipality </label>
						<input type="text" id="municipality" name="municipality" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_municipality"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Baranggay </label>
						<input type="text" id="baranggay" name="baranggay" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_baranggay"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Purok Street </label>
						<input type="text" id="purok_street" name="purok_street" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_purok_street"></div>
					</div>
					<div class="form-group col-sm-">
						<label>House Number </label>
						<input type="text" id="house_number" name="house_number" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_house_number"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Postal Code </label>
						<input type="number" id="postal_code" name="postal_code" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_postal_code"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Type Of Id </label>
						<input type="text" id="type_of_id" name="type_of_id" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_type_of_id"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Id Number </label>
						<input type="text" id="id_number" name="id_number" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_id_number"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Id Picture </label>
						<input type="text" id="id_picture" name="id_picture" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_id_picture"></div>
					</div>
					<div class="form-group col-sm-">
						<label>User Type </label>
						<input type="number" id="user_type" name="user_type" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_user_type"></div>
					</div>
					<div class="form-group col-sm-">
						<label>Contact Number </label>
						<input type="number" id="contact_number" name="contact_number" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_contact_number"></div>
					</div>

					<div class="col-sm-12 text-right">
                    	<button class="btn btn-secondary" type="submit">Save</button>
                  	</div>
				</div>
			</form>

          </div>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'user'])
</html>

<!-- Javascript Function-->
<script>

	function form_reset(){
    $("#user_id").val('');
        document.getElementById('profile_form_id').reset();
    }

    function add_profile(){
        $("#modal_add_profile").modal('show');
    }


	var tbl_profile;
	function show_profile(){
		if (tbl_profile) {
			tbl_profile.destroy();
		}
		var url = main_path + '/profile/list_profile';
		tbl_profile = $('#tbl_profile').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "slug_id",
		"title": "Slug_id",
	},{
		className: '',
		"data": "qrcode_path",
		"title": "Qrcode_path",
	},{
		className: '',
		"data": "firstname",
		"title": "Firstname",
	},{
		className: '',
		"data": "middlename",
		"title": "Middlename",
	},{
		className: '',
		"data": "lastname",
		"title": "Lastname",
	},{
		className: '',
		"data": "birthdate",
		"title": "Birthdate",
	},{
		className: '',
		"data": "gender",
		"title": "Gender",
	},{
		className: '',
		"data": "municipality",
		"title": "Municipality",
	},{
		className: '',
		"data": "baranggay",
		"title": "Baranggay",
	},{
		className: '',
		"data": "purok_street",
		"title": "Purok_street",
	},{
		className: '',
		"data": "house_number",
		"title": "House_number",
	},{
		className: '',
		"data": "postal_code",
		"title": "Postal_code",
	},{
		className: '',
		"data": "type_of_id",
		"title": "Type_of_id",
	},{
		className: '',
		"data": "id_number",
		"title": "Id_number",
	},{
		className: '',
		"data": "id_picture",
		"title": "Id_picture",
	},{
		className: '',
		"data": "user_type",
		"title": "User_type",
	},{
		className: '',
		"data": "contact_number",
		"title": "Contact_number",
	},{
		className: 'width-option-1 text-center',
		"data": "user_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_profile(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_profile(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#profile_form_id").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'profile_form_id');
					$('#modal_add_profile').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					show_profile();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'profile_form_id');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_profile(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/profile/delete_profile/' + data.user_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this profile?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					show_profile();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_profile(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#user_id').val(data.user_id);
		$('#slug_id').val(data.slug_id);
		$('#qrcode_path').val(data.qrcode_path);
		$('#firstname').val(data.firstname);
		$('#middlename').val(data.middlename);
		$('#lastname').val(data.lastname);
		$('#birthdate').val(data.birthdate);
		$('#gender').val(data.gender);
		$('#municipality').val(data.municipality);
		$('#baranggay').val(data.baranggay);
		$('#purok_street').val(data.purok_street);
		$('#house_number').val(data.house_number);
		$('#postal_code').val(data.postal_code);
		$('#type_of_id').val(data.type_of_id);
		$('#id_number').val(data.id_number);
		$('#id_picture').val(data.id_picture);
		$('#user_type').val(data.user_type);
		$('#contact_number').val(data.contact_number);
		$("#modal_add_profile").modal('show');
	}
</script>