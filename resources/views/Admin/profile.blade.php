<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Profile', 'icon' => asset('img/logo.png') ])
<body class="sidebar-mini layout-fixed" onload="show_adminprofile();">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'admin'])
    <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-sm-12 mt-3">
                <div class="card">
                    <div class="card-header h4"><i class="fa fa-user"></i> <span>Users Profile</span>
						<button class="btn btn-primary btn-sm float-right" onclick="form_reset(); add_profile()"><i class="fa fa-plus"></i> Add User</button>
               		</div>
					<div class="card-body">
					<table class="table table-bordered dt-responsive nowrap" id="tbl_adminprofile" style="width: 100%;"></table>
				    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
            </div>
        </div>
      </section>
    </div>
  </div>
</body>

 <div class="modal fade" role="dialog" id="modal_add_profile">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            User Profile
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
		  <form class="needs-validation" id="profile_form_id" action="{{ url('adminprofile/add_adminprofile') }}" novalidate>
				<div class="form-row">
					<input type="hidden" id="profile_id" name="profile_id" placeholder="" class="form-control" required>
					<!-- <div class="form-group col-sm-12">
						<label>Slug Id </label>
						<input type="text" id="slug_id" name="slug_id" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_slug_id"></div>
					</div> -->
					<!-- <div class="form-group col-sm-12">
						<label>Qrcode Path </label>
						<input type="text" id="qrcode_path" name="qrcode_path" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_qrcode_path"></div>
					</div> -->
					<div class="form-group col-sm-12">
						<label>Firstname </label>
						<input type="text" id="firstname" name="firstname" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_firstname"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Middlename </label>
						<input type="text" id="middlename" name="middlename" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_middlename"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Lastname </label>
						<input type="text" id="lastname" name="lastname" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_lastname"></div>
					</div>
					
					<div class="form-group col-sm-12">
						<label>Birthdate </label>
						<input type="date" id="birthdate" name="birthdate" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_birthdate"></div>
					</div>
					<label>Gender </label>
						<div class="form-group col-sm-12">
						<select id="gender" name="gender" class="form-control ">
							<option value="Male">Male</option>
							<option value="Female">Female</option>	
						</select>
						<div class="invalid-feedback" id="err_gender"></div>
					</div>
						<div class="form-group col-sm-12">
						<label>Municipality </label>
						<input type="text" id="municipality" name="municipality" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_municipality"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Baranggay </label>
						<input type="text" id="baranggay" name="baranggay" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_baranggay"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Purok Street </label>
						<input type="text" id="purok_street" name="purok_street" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_purok_street"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>House Number </label>
						<input type="text" id="house_number" name="house_number" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_house_number"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Postal Code </label>
						<input type="number" id="postal_code" name="postal_code" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_postal_code"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Type Of Id </label>
						<input type="text" id="type_of_id" name="type_of_id" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_type_of_id"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Id Number </label>
						<input type="text" id="id_number" name="id_number" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_id_number"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Id Picture </label>
						<input type="text" id="id_picture" name="id_picture" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_id_picture"></div>
					</div>
					<!-- <div class="form-group col-sm-12">
						<label>User Type </label>
						<input type="number" id="user_type" name="user_type" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_user_type"></div>
					</div> -->
					<div class="form-group col-sm-12">
						<label>Email Address </label>
						<input type="email" id="email_address" name="email_address" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_email_address"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Contact Number </label>
						<input type="number" id="contact_number" name="contact_number" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_contact_number"></div>
					</div>

					<div class="col-sm-12 text-right">
                    	<button class="btn btn-secondary" type="submit">Save</button>
                  	</div>
				</div>
			</form>

          </div>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>

<!-- Javascript Function-->
<script>

	function form_reset(){
    $("#profile_id").val('');
        document.getElementById('profile_form_id').reset();
    }

    function add_profile(){
        $("#modal_add_profile").modal('show');
    }


	var tbl_adminprofile;
	function show_adminprofile(){
		if (tbl_adminprofile) {
			tbl_adminprofile.destroy();
		}
		var url = main_path + '/adminprofile/list_adminprofile';
		tbl_adminprofile = $('#tbl_adminprofile').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [
	{
	className: 'width-option-1 text-center',
	"data": "qrcode_path",
	"orderable": false,
	"title": "QrCode",
		"render": function(data, type, row, meta){
				newdata1 = row.qrcode_path;

				return newdata1;
		}
	},
	{
		className: '',
		"data": "fullname",
		"title": "Name",
	},{
		className: '',
		"data": "municipality",
		"title": "Municipality",
	},{
		className: '',
		"data": "contact_number",
		"title": "Contact number",
	},{
		className: 'width-option-1 text-center',
		"data": "profile_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_adminprofile(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_adminprofile(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#profile_form_id").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'profile_form_id');
					$('#modal_add_profile').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					show_adminprofile();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'profile_form_id');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_adminprofile(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/adminprofile/delete_adminprofile/' + data.profile_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this profile?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					show_adminprofile();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_adminprofile(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#profile_id').val(data.profile_id);
		// $('#slug_id').val(data.slug_id);
		// $('#qrcode_path').val(data.qrcode_path);
		$('#firstname').val(data.firstname);
		$('#middlename').val(data.middlename);
		$('#lastname').val(data.lastname);
		$('#birthdate').val(data.birthdate);
		$('#gender').val(data.gender);
		$('#municipality').val(data.municipality);
		$('#baranggay').val(data.baranggay);
		$('#purok_street').val(data.purok_street);
		$('#house_number').val(data.house_number);
		$('#postal_code').val(data.postal_code);
		$('#type_of_id').val(data.type_of_id);
		$('#id_number').val(data.id_number);
		$('#id_picture').val(data.id_picture);
		// $('#user_type').val(data.user_type);
		$('#email_address').val(data.email_address);
		$('#contact_number').val(data.contact_number);
		$("#modal_add_profile").modal('show');
	}
</script>