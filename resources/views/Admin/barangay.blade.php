<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Barangay', 'icon' => asset('img/logo.png') ])
<body class="sidebar-mini layout-fixed" onload="show_adminbarangay();">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'admin'])
    <div class="content-wrapper">
     <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-sm-12 mt-3">
                <div class="card">
                    <div class="card-header h4"><i class="fa fa-house-user"></i> <span>Barangay Management</span>
						<button class="btn btn-primary btn-sm float-right" onclick="form_reset(); add_barangay()"><i class="fa fa-plus"></i> Add Barangay</button>
               		</div>
					<div class="card-body">
					<table class="table table-bordered dt-responsive nowrap" id="tbl_adminbarangay" style="width: 100%;"></table>
				    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
            </div>
        </div>
      </section>
    </div>
  </div>
</body>

<div class="modal fade" role="dialog" id="modal_add_barangay">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Barangay Information
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
		  <form class="needs-validation" id="barangay_form_id" action="{{ url('adminbarangay/add_adminbarangay') }}" novalidate>
				<div class="form-row">
					<input type="hidden" id="barangay_id" name="barangay_id" placeholder="" class="form-control" required>
					<!-- <div class="form-group col-sm-12">
						<label>Slug Id </label>
						<input type="text" id="slug_id" name="slug_id" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_slug_id"></div>
					</div> -->
					<div class="form-group col-sm-12">
						<label>Barangay Representative </label>
						<input type="text" id="barangay_representative" name="barangay_representative" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_barangay_representative"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Municipality </label>
						<input type="text" id="municipality" name="municipality" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_municipality"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Barangay </label>
						<input type="text" id="barangay" name="barangay" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_barangay"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Country </label>
						<input type="text" id="country" name="country" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_country"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Postal Code </label>
						<input type="number" id="postal_code" name="postal_code" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_postal_code"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Contact Number </label>
						<input type="number" id="contact_number" name="contact_number" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_contact_number"></div>
					</div>

				 <div class="col-sm-12 text-right">
					<button class="btn btn-secondary" type="submit">Save</button>
                 </div>
				</div>
		   </form>
          </div>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div>


  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>


<!-- Javascript Function-->
<script>

	function form_reset(){
    $("#barangay_id").val('');
        document.getElementById('barangay_form_id').reset();
    }

    function add_barangay(){
        $("#modal_add_barangay").modal('show');
    }

	var tbl_adminbarangay;
	function show_adminbarangay(){
		if (tbl_adminbarangay) {
			tbl_adminbarangay.destroy();
		}
		var url = main_path + '/adminbarangay/list_adminbarangay';
		tbl_adminbarangay = $('#tbl_adminbarangay').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [
	{
		className: '',
		"data": "barangay_representative",
		"title": "Barangay representative",
	},{
		className: '',
		"data": "municipality",
		"title": "Municipality",
	},{
		className: '',
		"data": "barangay",
		"title": "Barangay",
	},{
		className: '',
		"data": "country",
		"title": "Country",
	},{
		className: '',
		"data": "contact_number",
		"title": "Contact number",
	},{
		className: 'width-option-1 text-center',
		"data": "barangay_id ",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_adminbarangay(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_adminbarangay(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#barangay_form_id").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'barangay_form_id');
					$('#modal_add_barangay').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					show_adminbarangay();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'barangay_form_id');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_adminbarangay(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/adminbarangay/delete_adminbarangay/' + data.barangay_id ;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this Barangay?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					show_adminbarangay();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_adminbarangay(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#barangay_id').val(data.barangay_id);
		$('#slug_id').val(data.slug_id);
		$('#barangay_representative').val(data.barangay_representative);
		$('#municipality').val(data.municipality);
		$('#barangay').val(data.barangay);
		$('#country').val(data.country);
		$('#postal_code').val(data.postal_code);
		$('#contact_number').val(data.contact_number);
		$("#modal_add_barangay").modal('show');
	}
</script>