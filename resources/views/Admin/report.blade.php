<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Reports', 'icon' => asset('img/logo.png') ])
<body class="sidebar-mini layout-fixed">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'admin'])
    <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <h1>Report Index</h1>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>