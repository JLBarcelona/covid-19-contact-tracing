<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Store', 'icon' => asset('img/logo.png') ])
<body class="sidebar-mini layout-fixed" onload="show_adminstore();">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'admin'])
    <div class="content-wrapper">
     <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-sm-12 mt-3">
                <div class="card">
                    <div class="card-header h4"><i class="fa fa-store-alt"></i> <span>Store Management</span>
						<button class="btn btn-primary btn-sm float-right" onclick="form_reset(); add_store()"><i class="fa fa-plus"></i> Add Store</button>
               		</div>
					<div class="card-body">
					<table class="table table-bordered dt-responsive nowrap" id="tbl_adminstore" style="width: 100%;"></table>
				    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
            </div>
        </div>
      </section>
    </div>
  </div>
</body>

<div class="modal fade" role="dialog" id="modal_add_store">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Store Information
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="store_form_id" action="{{ url('adminstore/add_adminstore') }}" novalidate>
              <div class="form-row">
                <input type="hidden" id="store_id" name="store_id" placeholder="" class="form-control" required>
                <!-- <div class="form-group col-sm-12">
                  <label>Slug Id </label>
                  <input type="text" id="slug_id" name="slug_id" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_slug_id"></div>
                </div> -->
                <div class="form-group col-sm-12">
                  <label>Store Qr Path </label>
                  <input type="text" id="store_qr_path" name="store_qr_path" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_store_qr_path"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Store Name </label>
                  <input type="text" id="store_name" name="store_name" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_store_name"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Store Address </label>
                  <input type="text" id="store_address" name="store_address" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_store_address"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Store Representative </label>
                  <input type="text" id="store_representative" name="store_representative" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_store_representative"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Contact Number </label>
                  <input type="number" id="contact_number" name="contact_number" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_contact_number"></div>
                </div>

                <div class="col-sm-12 text-right">
                  <button class="btn btn-secondary" type="submit">Save</button>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div>


  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>


<!-- Javascript Function-->
<script>

    function form_reset(){
    $("#store_id").val('');
        document.getElementById('store_form_id').reset();
    }

    function add_store(){
        $("#modal_add_store").modal('show');
    }


	var tbl_adminstore;
	function show_adminstore(){
		if (tbl_adminstore) {
			tbl_adminstore.destroy();
		}
		var url = main_path + '/adminstore/list_adminstore';
		tbl_adminstore = $('#tbl_adminstore').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [
  //     {
	// 	className: '',
	// 	"data": "slug_id",
	// 	"title": "Slug_id",
	// },{
	// 	className: '',
	// 	"data": "store_qr_path",
	// 	"title": "Store_qr_path",
	// },
  {
		className: '',
		"data": "store_name",
		"title": "Store name",
	},{
		className: '',
		"data": "store_address",
		"title": "Store address",
	},{
		className: '',
		"data": "store_representative",
		"title": "Store representative",
	},{
		className: '',
		"data": "contact_number",
		"title": "Contact number",
	},{
		className: 'width-option-1 text-center',
		"data": "store_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_adminstore(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_adminstore(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#store_form_id").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'store_form_id');
          $('#modal_add_store').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					show_adminstore();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'store_form_id');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_adminstore(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/adminstore/delete_adminstore/' + data.store_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this store?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
          show_adminstore();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_adminstore(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#store_id').val(data.store_id);
		$('#slug_id').val(data.slug_id);
		$('#store_qr_path').val(data.store_qr_path);
		$('#store_name').val(data.store_name);
		$('#store_address').val(data.store_address);
		$('#store_representative').val(data.store_representative);
		$('#contact_number').val(data.contact_number);
    $("#modal_add_store").modal('show');
	}
</script>