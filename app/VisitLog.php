<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitLog extends Model
{
    protected $primaryKey = 'logs_id';

 	protected $table = 'cv_visit_logs';

 	protected $fillable = [
      'store_id','store_type','owner_id','owner_type','temperature','visit_in_date','visit_out_date','created_at','updated_at','deleted_at'
      ];
}
