<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
     protected $primaryKey = 'logs_id';

 	protected $table = 'cv_visit_logs';

 	protected $fillable = [
     'temperature','visit_in_date','visit_out_date','created_at','updated_at','deleted_at'
      ];


    protected $hidden = [
        'owner_id', 'owner_type', 'store_id', 'store_type'
    ];

}
