<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User;
use Illuminate\Support\Facades\Hash;

class StoreController extends Controller
{
  function index(){
  	return view('Store.index');
  }

  function scanner(){
  	return view('Store.scanner');
  }
}
