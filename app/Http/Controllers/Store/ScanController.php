<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Report;
use Auth;
use Validator;
use App\User;
use App\Profile;

class ScanController extends Controller
{
    function add_report(Request $request){
		$store = Auth::user()->owner;

		$temperature = $request->get('temperature');
		$slug_id = $request->get('slug_id');

		$profile = Profile::where('slug_id', $slug_id);

		$validator = Validator::make($request->all(), [
			'temperature' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{

				if ($profile->count() > 0) {
					$_profile = $profile->first();

					$report = new Report;
					// store
					$report->store_id = $store->store_id;
					$report->store_type = get_class($store);
					// owner
					$report->owner_id = $_profile->profile_id;
					$report->owner_type = get_class($_profile);
					// Temp
					$report->temperature = $temperature;
					$report->visit_in_date = now();
					
					if($report->save()){
						return response()->json(['status' => true, 'message' => 'Log saved successfully!']);
					}
				}
		}
	}



	function add_scan(Request $request){
		$user_code = $request->get('user_code');

		$validator = Validator::make($request->all(), [
			'user_code' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			$check = Profile::where('slug_id', $user_code);

			if ($check->count() > 0) {
				return response()->json(['status' => true, 'data' => $check->first()]);
			}else{
				return response()->json(['status' => false, 'message' => 'Code not found!']);
			}

		}
	}


}
