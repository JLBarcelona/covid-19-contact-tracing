<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Barangay;

class BarangayController extends Controller
{
  function barangay(){
    return view('Admin.barangay');
  }

function list_adminbarangay(){
	$adminbarangay = Barangay::whereNull('deleted_at')->get();
	return response()->json(['status' => true, 'data' => $adminbarangay]);
}

function add_adminbarangay(Request $request){
	$barangay_id = $request->get('barangay_id');
	// $slug_id = $request->get('slug_id');
	$barangay_representative = $request->get('barangay_representative');
	$municipality = $request->get('municipality');
	$barangay = $request->get('barangay');
	$country = $request->get('country');
	$postal_code = $request->get('postal_code');
	$contact_number = $request->get('contact_number');

	$validator = Validator::make($request->all(), [
		// 'slug_id' => 'required',
		'barangay_representative' => 'required',
		'municipality' => 'required',
		'barangay' => 'required',
		'country' => 'required',
		'postal_code' => 'required',
		'contact_number' => 'required',
	]);

	if ($validator->fails()) {
		return response()->json(['status' => false, 'error' => $validator->errors()]);
	}else{
		if (!empty($barangay_id)) {
			$adminbarangay = Barangay::find($barangay_id);
			// $adminbarangay->slug_id = $slug_id;
			$adminbarangay->barangay_representative = $barangay_representative;
			$adminbarangay->municipality = $municipality;
			$adminbarangay->barangay = $barangay;
			$adminbarangay->country = $country;
			$adminbarangay->postal_code = $postal_code;
			$adminbarangay->contact_number = $contact_number;
			if($adminbarangay->save()){
				return response()->json(['status' => true, 'message' => 'Barangay updated successfully!']);
			}
		}else{
			$adminbarangay = new Barangay;
			// $adminbarangay->slug_id = $slug_id;
			$adminbarangay->barangay_representative = $barangay_representative;
			$adminbarangay->municipality = $municipality;
			$adminbarangay->barangay = $barangay;
			$adminbarangay->country = $country;
			$adminbarangay->postal_code = $postal_code;
			$adminbarangay->contact_number = $contact_number;
			if($adminbarangay->save()){
				return response()->json(['status' => true, 'message' => 'Barangay saved successfully!']);
			}
		}
	}
}

function delete_adminbarangay($barangay_id ){
	$adminbarangay = Barangay::find($barangay_id );
	if($adminbarangay->delete()){
		return response()->json(['status' => true, 'message' => 'Barangay deleted successfully!']);
	}
}

}
