<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Store;

class StoreController extends Controller
{
    function store(){
        return view('Admin.store');
    }

  function list_adminstore(){
    $adminstore = Store::whereNull('deleted_at')->get();
    return response()->json(['status' => true, 'data' => $adminstore]);
  }
  
  function add_adminstore(Request $request){
    $store_id= $request->get('store_id');
    // $slug_id = $request->get('slug_id');
    $store_qr_path = $request->get('store_qr_path');
    $store_name = $request->get('store_name');
    $store_address = $request->get('store_address');
    $store_representative = $request->get('store_representative');
    $contact_number = $request->get('contact_number');
  
    $validator = Validator::make($request->all(), [
      // 'slug_id' => 'required',
      'store_qr_path' => 'required',
      'store_name' => 'required',
      'store_address' => 'required',
      'store_representative' => 'required',
      'contact_number' => 'required',
    ]);
  
    if ($validator->fails()) {
      return response()->json(['status' => false, 'error' => $validator->errors()]);
    }else{
      if (!empty($store_id)) {
        $adminstore = Store::find($store_id);
        // $adminstore->slug_id = $slug_id;
        $adminstore->store_qr_path = $store_qr_path;
        $adminstore->store_name = $store_name;
        $adminstore->store_address = $store_address;
        $adminstore->store_representative = $store_representative;
        $adminstore->contact_number = $contact_number;
        if($adminstore->save()){
          return response()->json(['status' => true, 'message' => 'Store updated successfully!']);
        }
      }else{
        $adminstore = new Store;
        // $adminstore->slug_id = $slug_id;
        $adminstore->store_qr_path = $store_qr_path;
        $adminstore->store_name = $store_name;
        $adminstore->store_address = $store_address;
        $adminstore->store_representative = $store_representative;
        $adminstore->contact_number = $contact_number;
        if($adminstore->save()){
          return response()->json(['status' => true, 'message' => 'Store saved successfully!']);
        }
      }
    }
  }
  
  function delete_adminstore($store_id){
    $adminstore = Store::find($store_id);
    if($adminstore->delete()){
      return response()->json(['status' => true, 'message' => 'Store deleted successfully!']);
    }
  }

}
