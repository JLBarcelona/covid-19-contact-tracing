<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Profile;
use App\User;
use Str;

class ProfileController extends Controller
{
    function profile(){
        return view('Admin.profile');
    }

  function create_account($profile,$email_address){
    $user_check = Profile::where('profile_id', $profile->profile_id);

    $pro = $user_check->first();
    $user_profile_id = (!empty($pro->owner->user_id)) ? $pro->owner->user_id : ''; 
    
    $user_count = User::where('user_id', $user_profile_id);

    if ($user_count->count() > 0) {
      $user = $user_count->first();
      $user->email_address = $email_address;
      $user->owner_id = $profile->profile_id;
      $user->owner_type = get_class($profile);
      $user->username = Str::lower($profile->firstname).'_'.Str::lower($profile->lastname);
      $user->password = Str::lower($profile->middlename);
      $user->save();
      $this->create_slug($user->user_id, $profile->profile_id);
    }else{
       $user = new User;
        $user->email_address = $email_address;
        $user->owner_id = $profile->profile_id;
        $user->verified = 1;
        $user->user_type = 4;
        $user->confirm = 1;
        $user->owner_type = get_class($profile);
        $user->username = Str::lower($profile->firstname).'_'.Str::lower($profile->lastname);
        $user->password = Str::lower($profile->middlename);

        if ($user->save()) {
          $this->create_slug($user->user_id, $profile->profile_id);
        }
    }

  }

  function create_slug($user_id, $profile_id){
    $profile = Profile::find($profile_id);
    if (!empty($profile->slug_id)) {
      
    }else{
      $str = Str::random(2).$user_id.$profile_id.Str::random(1).$user_id;
       $profile->slug_id = substr($str, 0, 6);
    }
    
    if ($profile->save()) {
      return true;
    }
  }


  function list_adminprofile(){
    $profile = Profile::whereNull('deleted_at')->get();
    return response()->json(['status' => true, 'data' => $profile]);
  }
  
  function add_adminprofile(Request $request){
    $profile_id= $request->get('profile_id');
    // $slug_id = $request->get('slug_id');
    // $qrcode_path = $request->get('qrcode_path');
    $firstname = $request->get('firstname');
    $middlename = $request->get('middlename');
    $lastname = $request->get('lastname');
    $birthdate = $request->get('birthdate');
    $gender = $request->get('gender');
    $municipality = $request->get('municipality');
    $baranggay = $request->get('baranggay');
    $purok_street = $request->get('purok_street');
    $house_number = $request->get('house_number');
    $postal_code = $request->get('postal_code');
    $type_of_id = $request->get('type_of_id');
    $id_number = $request->get('id_number');
    $id_picture = $request->get('id_picture');
    $user_type = $request->get('user_type');
    $contact_number = $request->get('contact_number');
    $email_address = $request->get('email_address');
  
    $validator = Validator::make($request->all(), [
      // 'slug_id' => 'required',
      // 'qrcode_path' => 'required',
      'firstname' => 'required',
      'middlename' => 'required',
      'lastname' => 'required',
      'birthdate' => 'required',
      'gender' => 'required',
      'municipality' => 'required',
      'baranggay' => 'required',
      'purok_street' => 'required',
      'house_number' => 'required',
      'postal_code' => 'required',
      'type_of_id' => 'required',
      'id_number' => 'required',
      'id_picture' => 'required',
      'email_address' => 'email',
      'contact_number' => 'required',
    ]);
  
    if ($validator->fails()) {
      return response()->json(['status' => false, 'error' => $validator->errors()]);
    }else{
      if (!empty($profile_id)) {
        $profile = Profile::find($profile_id);
        // $profile->slug_id = $slug_id;
        // $profile->qrcode_path = $qrcode_path;
        $profile->firstname = $firstname;
        $profile->middlename = $middlename;
        $profile->lastname = $lastname;
        $profile->birthdate = $birthdate;
        $profile->gender = $gender;
        $profile->municipality = $municipality;
        $profile->baranggay = $baranggay;
        $profile->purok_street = $purok_street;
        $profile->house_number = $house_number;
        $profile->postal_code = $postal_code;
        $profile->type_of_id = $type_of_id;
        $profile->id_number = $id_number;
        $profile->id_picture = $id_picture;
        $profile->contact_number = $contact_number;
        if($profile->save()){
          $this->create_account($profile, $email_address);
          return response()->json(['status' => true, 'message' => 'Profile updated successfully!']);
        }
      }else{
        $profile = new Profile;
        // $profile->slug_id = $slug_id;
        // $profile->qrcode_path = $qrcode_path;
        $profile->firstname = $firstname;
        $profile->middlename = $middlename;
        $profile->lastname = $lastname;
        $profile->birthdate = $birthdate;
        $profile->gender = $gender;
        $profile->municipality = $municipality;
        $profile->baranggay = $baranggay;
        $profile->purok_street = $purok_street;
        $profile->house_number = $house_number;
        $profile->postal_code = $postal_code;
        $profile->type_of_id = $type_of_id;
        $profile->id_number = $id_number;
        $profile->id_picture = $id_picture;
        $profile->user_type = 4;
       
        $profile->contact_number = $contact_number;
        if($profile->save()){
          $this->create_account($profile, $email_address);
          return response()->json(['status' => true, 'message' => 'Profile saved successfully!']);
        }
      }
    }
  }
  
  function delete_adminprofile($user_id){
    $profile = Profile::find($user_id);
    if($profile->delete()){
      return response()->json(['status' => true, 'message' => 'Profile deleted successfully!']);
    }
  }

}
