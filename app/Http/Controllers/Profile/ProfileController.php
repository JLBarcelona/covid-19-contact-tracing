<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Profile;

class ProfileController extends Controller
{
  function index(){
  	return view('Profile.index');
  }

  function list_profile(){
    $profile = Profile::whereNull('deleted_at')->get();
    return response()->json(['status' => true, 'data' => $profile]);
  }
  
  function add_profile(Request $request){
    $user_id= $request->get('user_id');
    $slug_id = $request->get('slug_id');
    $qrcode_path = $request->get('qrcode_path');
    $firstname = $request->get('firstname');
    $middlename = $request->get('middlename');
    $lastname = $request->get('lastname');
    $birthdate = $request->get('birthdate');
    $gender = $request->get('gender');
    $municipality = $request->get('municipality');
    $baranggay = $request->get('baranggay');
    $purok_street = $request->get('purok_street');
    $house_number = $request->get('house_number');
    $postal_code = $request->get('postal_code');
    $type_of_id = $request->get('type_of_id');
    $id_number = $request->get('id_number');
    $id_picture = $request->get('id_picture');
    $user_type = $request->get('user_type');
    $contact_number = $request->get('contact_number');
  
    $validator = Validator::make($request->all(), [
      'slug_id' => 'required',
      'qrcode_path' => 'required',
      'firstname' => 'required',
      'middlename' => 'required',
      'lastname' => 'required',
      'birthdate' => 'required',
      'gender' => 'required',
      'municipality' => 'required',
      'baranggay' => 'required',
      'purok_street' => 'required',
      'house_number' => 'required',
      'postal_code' => 'required',
      'type_of_id' => 'required',
      'id_number' => 'required',
      'id_picture' => 'required',
      'user_type' => 'required',
      'contact_number' => 'required',
    ]);
  
    if ($validator->fails()) {
      return response()->json(['status' => false, 'error' => $validator->errors()]);
    }else{
      if (!empty($user_id)) {
        $profile = Profile::find($user_id);
        $profile->slug_id = $slug_id;
        $profile->qrcode_path = $qrcode_path;
        $profile->firstname = $firstname;
        $profile->middlename = $middlename;
        $profile->lastname = $lastname;
        $profile->birthdate = $birthdate;
        $profile->gender = $gender;
        $profile->municipality = $municipality;
        $profile->baranggay = $baranggay;
        $profile->purok_street = $purok_street;
        $profile->house_number = $house_number;
        $profile->postal_code = $postal_code;
        $profile->type_of_id = $type_of_id;
        $profile->id_number = $id_number;
        $profile->id_picture = $id_picture;
        $profile->user_type = $user_type;
        $profile->contact_number = $contact_number;
        if($profile->save()){
          return response()->json(['status' => true, 'message' => 'Profile updated successfully!']);
        }
      }else{
        $profile = new Profile;
        $profile->slug_id = $slug_id;
        $profile->qrcode_path = $qrcode_path;
        $profile->firstname = $firstname;
        $profile->middlename = $middlename;
        $profile->lastname = $lastname;
        $profile->birthdate = $birthdate;
        $profile->gender = $gender;
        $profile->municipality = $municipality;
        $profile->baranggay = $baranggay;
        $profile->purok_street = $purok_street;
        $profile->house_number = $house_number;
        $profile->postal_code = $postal_code;
        $profile->type_of_id = $type_of_id;
        $profile->id_number = $id_number;
        $profile->id_picture = $id_picture;
        $profile->user_type = $user_type;
        $profile->contact_number = $contact_number;
        if($profile->save()){
          return response()->json(['status' => true, 'message' => 'Profile saved successfully!']);
        }
      }
    }
  }
  
  
  function delete_profile($user_id){
    $profile = Profile::find($user_id);
    if($profile->delete()){
      return response()->json(['status' => true, 'message' => 'Profile deleted successfully!']);
    }
  }
}
