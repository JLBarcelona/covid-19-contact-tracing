<?php

namespace App\Http\Controllers\Barangay;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User;
use App\Profile;
use App\Barangay;

class BarangayController extends Controller
{
    function index(){
  	return view('Barangay.index');
  }

  function pending(){
  	return view('Barangay.pending');
  }

  function list_pending(){
    $user = Auth::user()->owner;
    $brgy = $user->barangay;

    $pending_user = Profile::where('user_type','4')->where('baranggay', $brgy)->whereHas('owner', function ($query){
      return $query->where('confirm', 0);
    })->get();
	  return response()->json(['status' => true, 'data' => $pending_user]);
  }

  function approved(){
  	return view('Barangay.approved');
  }

  function list_approved(){
   	$user = Auth::user()->owner;
    $brgy = $user->barangay;

    $approved_user = Profile::where('user_type','4')->where('baranggay', $brgy)->whereHas('owner', function ($query){
      return $query->where('confirm', 1);
    })->get();
	 
      return response()->json(['status' => true, 'data' => $approved_user]);
  }

  function disapproved(){
    return view('Barangay.disapproved');
  }

  function list_disapproved(){
    $user = Auth::user()->owner;
    $brgy = $user->barangay;

    $pending_user = Profile::where('user_type','4')->where('baranggay', $brgy)->whereHas('owner', function ($query){
      return $query->where('confirm', 2);
    })->get();
    return response()->json(['status' => true, 'data' => $pending_user]);

  }

  function approve_disapprove_pending(Request $request){
    $slug_id = $request->get('user_id');
    $status = $request->get('status');

    $user = Profile::where('slug_id', $slug_id);

    if ($user->count() > 0) {
      $usr = $user->first();
      $account = $usr->owner;
      $account->confirm = $status;
      if ($account->save()) {
        return response()->json(['status' => true, 'message' => 'Profile approved successfully!']);
      }else{
        return response()->json(['status' => false, 'message' => 'Profile ID not found!']);
      }
    }


  }

  function delete_profile($user_id){
    $profile = Profile::find($user_id);
    if($profile->delete()){
      return response()->json(['status' => true, 'message' => 'Profile deleted successfully!']);
    }
  }
}
