<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class Admin extends Model
{
 protected $primaryKey = 'admin_id';

  protected $table = 'cv_admin';

  protected $fillable = [
      'slug_id','admin_name','created_at','updated_at','deleted_at'
      ];

	public function owner(){
		return $this->morphOne('App\User', 'owner')->whereNull('deleted_at');
	}

	public function setSlugIdAttribute(){
        $this->attributes['slug_id'] = Str::random(7).$this->attributes['store_id'].Str::random(7);
    }
}
