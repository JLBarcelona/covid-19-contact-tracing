<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class Barangay extends Model
{
    protected $primaryKey = 'barangay_id';

 	protected $table = 'cv_barangay';

 	protected $fillable = [
      'slug_id','barangay_representative','municipality','barangay','country', 'contact_number','postal_code','created_at','updated_at','deleted_at'
      ];

	  public function owner(){
			return $this->morphOne('App\User', 'owner')->whereNull('deleted_at');
	  }

	  public function setSlugIdAttribute(){
        $this->attributes['slug_id'] = Str::random(7).$this->attributes['barangay_id'].Str::random(7);
    }
}
