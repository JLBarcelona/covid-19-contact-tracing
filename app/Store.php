<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class Store extends Model
{
  protected $primaryKey = 'store_id';

  protected $table = 'cv_store';

  protected $appends = ['store_logo'];

  protected $fillable = [
      'slug_id', 'store_logo','store_qr_path','store_name','store_address','store_representative','created_at','updated_at','deleted_at'
      ];

	public function owner(){
		return $this->morphOne('App\User', 'owner')->whereNull('deleted_at');
	}

  public function getStoreLogoAttribute(){
    if (!empty($this->store_logo)) {
      return asset('store/'.$this->store_logo);
    }else{
      return asset('img/store.png');

    }
  }

	public function setSlugIdAttribute(){
        $this->attributes['slug_id'] = Str::random(7).$this->attributes['store_id'].Str::random(7);
    }
}
