<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Url;
use DNS1D;

class Profile extends Model
{
    protected $primaryKey = 'profile_id';

 	protected $table = 'cv_profile';

    protected $appends = ['email_address', 'fullname', 'avatar', 'user_detail', 'is_confirm'];

 	protected $fillable = [
      'slug_id','qrcode_path', 'profile_path','firstname','middlename','lastname','birthdate','gender','municipality','baranggay','purok_street','house_number','postal_code','type_of_id','id_number','email','contact_number','id_picture','user_type','created_at','updated_at','deleted_at'
      ];


   public function owner(){
        return $this->morphOne('App\User', 'owner')->whereNull('deleted_at');
    }


    public function getEmailAddressAttribute(){
    	$data =  $this->owner()->first();
    	
    	return $data->email_address;
    }

    public function getIsConfirmAttribute(){
        $data =  $this->owner()->first();
        
        return $data->confirm;
    }




    public function getUserDetailAttribute(){
        return $this->owner()->first();
        
    }


    public function getFullnameAttribute(){
        return $this->firstname.' '.$this->lastname;
    }

    public function getAvatarAttribute(){
        if (!empty($this->profile_path)) {
            return asset('profile/'.$this->profile_path);
        }else{
            return asset('img/avatar.png');
        }
    }

    public function getQrcodePathAttribute(){
        return '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($this->slug_id, 'C39E') . '" alt="barcode" width="190"  height="40"  onclick="barcode_fullscreen(this)" data-name="'.$this->fullname.'" />';
    	// return asset('qrcode/'.$this->attributes['qrcode_path']);
    }

}
