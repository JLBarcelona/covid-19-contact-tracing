<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::namespace('Account')->group(function () {
	Route::get('/', function () {
	    return view('Account.index');
	});

	Route::post('/check_user', 'AccountController@login_user');
	Route::get('/logout/admin','AccountController@logout_admin')->name('logout.admin');

	Route::get('/logout/user','AccountController@logout_user')->name('logout.user');

 	Route::get('/forgot_password', function(){
	    return view('Account.forgot_password');
	});

	Route::post('get_token/{user_type}', 'ForgotPasswordController@get_token');
	Route::get('/new_password/{token}/{id}', 'ForgotPasswordController@new_password')->name('account.new_password');
	Route::get('/verify_account/{token}/{id}', 'AccountController@verify_account')->name('account.verify');

	Route::post('/create_new_password/{token}/{id}', 'ForgotPasswordController@create_new_password');


	//Registration
	 Route::get('/register', 'AccountController@register_index')->name('profile.index');
	 Route::post('/register/add_register', 'AccountController@add_register')->name('register.add');
	 Route::post('/profile/add_profile', 'AccountController@add_profile')->name('profile.add');

	 
});


Route::namespace('Admin')->group(function () {
	Route::middleware(['auth'])->group(function () {
		Route::get('/system/settings', 'AdminController@settings')->name('admin.settings');

		Route::get('/admin', 'AdminController@index')->name('admin.index');
		Route::get('/admin/barangay', 'BarangayController@barangay')->name('admin.barangay');
		Route::get('/admin/store', 'StoreController@store')->name('admin.store');
		Route::get('/admin/profile', 'ProfileController@profile')->name('admin.profile');
		Route::get('/admin/report', 'ReportController@report')->name('admin.report');

		//Barangay
		Route::get('/adminbarangay/list_adminbarangay', 'BarangayController@list_adminbarangay')->name('adminbarangay.list');
		Route::post('/adminbarangay/add_adminbarangay', 'BarangayController@add_adminbarangay')->name('adminbarangay.add');
		Route::get('/adminbarangay/delete_adminbarangay/{barangay_id}', 'BarangayController@delete_adminbarangay')->name('adminbarangay.delete');

		//Store
		Route::get('/adminstore/list_adminstore', 'StoreController@list_adminstore')->name('adminstore.list');
		Route::post('/adminstore/add_adminstore', 'StoreController@add_adminstore')->name('adminstore.add');
		Route::get('/adminstore/delete_adminstore/{store_id}', 'StoreController@delete_adminstore')->name('adminstore.delete');

		//Profile
		Route::get('/adminprofile/list_adminprofile', 'ProfileController@list_adminprofile')->name('adminprofile.list');
		Route::post('/adminprofile/add_adminprofile', 'ProfileController@add_adminprofile')->name('adminprofile.add');
		Route::get('/adminprofile/delete_adminprofile/{user_id}', 'ProfileController@delete_adminprofile')->name('adminprofile.delete');

	});
});


Route::namespace('Profile')->group(function () {
	Route::middleware(['auth'])->group(function () {
		Route::get('/profile', 'ProfileController@index')->name('profile.index');
		Route::get('/profile/list_profile', 'ProfileController@list_profile')->name('profile.list');
		Route::post('/profile/add_profile', 'ProfileController@add_profile')->name('profile.add');
		Route::get('/profile/delete_profile/{user_id}', 'ProfileController@delete_profile')->name('profile.delete');
	});
});


Route::namespace('Store')->group(function () {
	Route::middleware(['auth'])->group(function () {
		Route::get('/store', 'StoreController@index')->name('store.index');
		Route::get('/scanner', 'StoreController@scanner')->name('store.scanner');

		Route::post('/report/add_report', 'ScanController@add_report')->name('report.add');
		// Scan
	 	Route::post('/scan/add_scan', 'ScanController@add_scan')->name('scan.add');

	});
});


Route::namespace('Barangay')->group(function () {
	Route::middleware(['auth'])->group(function () {
		Route::get('/barangay', 'BarangayController@index')->name('barangay.index');
		Route::get('/barangay/user/pending', 'BarangayController@pending')->name('barangay.pending');
		Route::get('/barangay/user/approved', 'BarangayController@approved')->name('barangay.approved');
		Route::get('/barangay/user/disapproved', 'BarangayController@disapproved')->name('barangay.disapproved');


		Route::get('/barangay/list_pending', 'BarangayController@list_pending')->name('barangay.listpending');
		Route::get('/barangay/list_approved', 'BarangayController@list_approved')->name('barangay.listapproved');
		Route::get('/barangay/list_disapproved', 'BarangayController@list_disapproved')->name('barangay.listdisapprove');

		Route::post('/barangay/mark_as_pending', 'BarangayController@mark_as_pending')->name('barangay.markaspending');

		Route::get('/account/approve_status/', 'BarangayController@approve_disapprove_pending')->name('approve.status');

		Route::get('/barangay/delete_profile/{user_id}', 'BarangayController@delete_profile')->name('barangayprofile.delete');


	});
});
