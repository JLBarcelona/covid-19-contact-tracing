function approve_pending_disapprove(approve, name, user_id){
  var message = '';
  if (approve == 1) {
    message = 'approve';
  }else if (approve == 2) {
    message = 'disapprove';
  }else if (approve == 0) {
    message = 'mark as pending';
  }

  var url = main_path + '/account/approve_status/';
  swal({
        title: "Are you sure?",
        text: "Do you want to "+message+" "+name+"?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
        function(){
         $.ajax({
          type:"GET",
          url:url,
          data:{user_id : user_id, status : approve},
          dataType:'json',
          beforeSend:function(){
          },
          success:function(response){
            // console.log(response);
           if (response.status == true) {
              swal("Success", response.message, "success");

              if (approve == 1) {
                show_pendinguser();
              }else if (approve == 2) {
                show_pendinguser();
              }else if (approve == 0) {
                show_disapuser();
                show_approveduser();
              }

           }else{
              swal("Error", response.message, "error");
            console.log(response);
           }
          },
          error: function(error){
            console.log(error);
          }
        });
      });
  }



   var tbl_pendinguser;
  function show_pendinguser(){
    if (tbl_pendinguser) {
      tbl_pendinguser.destroy();
    }
    var url = main_path + '/barangay/list_pending';
    tbl_pendinguser = $('#tbl_pendinguser').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [
  {
  className: 'width-option-1 text-center',
  "data": "qrcode_path",
  "orderable": false,
  "title": "Barcode",
    "render": function(data, type, row, meta){
        return row.qrcode_path;
    }
  },
  {
    className: '',
    "data": "fullname",
    "title": "Name",
  },{
    className: '',
    "data": "baranggay",
    "title": "Barangay",
  },{
    className: '',
    "data": "municipality",
    "title": "Municipality",
  },{
    className: '',
    "data": "contact_number",
    "title": "Contact number",
  },{
    className: 'width-option-1 text-center',
    "data": "profile_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        newdata += '<button class="btn btn-success btn-sm font-base mt-1" onclick="approve_pending_disapprove(\'1\', \''+row.fullname+'\', \''+row.slug_id+'\')" type="button"> Approve</button>';
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="approve_pending_disapprove(\'2\', \''+row.fullname+'\', \''+row.slug_id+'\')" type="button"> Disapprove</button>';
        return newdata;
       }
  }
  ]
  });
  }



   var tbl_disapuser;
  function show_disapuser(){
    if (tbl_disapuser) {
      tbl_disapuser.destroy();
    }
    var url = main_path + '/barangay/list_disapproved';
    tbl_disapuser = $('#tbl_disapuser').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [
  {
  className: 'width-option-1 text-center',
  "data": "qrcode_path",
  "orderable": false,
  "title": "Barcode",
    "render": function(data, type, row, meta){
        return row.qrcode_path;
    }
  },
  {
    className: '',
    "data": "fullname",
    "title": "Name",
  },{
    className: '',
    "data": "municipality",
    "title": "Municipality",
  },{
    className: '',
    "data": "contact_number",
    "title": "Contact number",
  },{
    className: 'width-option-1 text-center',
    "data": "profile_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        newdata += '<button class="btn btn-success btn-sm font-base mt-1" onclick="approve_pending_disapprove(\'0\', \''+row.fullname+'\', \''+row.slug_id+'\')" type="button"> Mark as Pending</button>';
        
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_profile(this)" type="button"> Delete</button>';
        return newdata;
       }
  }
  ]
  });
  }

  function delete_profile(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    var url =  main_path + '/barangay/delete_profile/' + data.profile_id;
      swal({
        title: "Are you sure?",
        text: "Do you want to delete this profile?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          swal("Success", response.message, "success");
          show_disapuser();
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }



  var tbl_approveduser;
  function show_approveduser(){
    if (tbl_approveduser) {
      tbl_approveduser.destroy();
    }
    var url = main_path + '/barangay/list_approved';
    tbl_approveduser = $('#tbl_approveduser').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [
  {
  className: 'width-option-1 text-center',
  "data": "qrcode_path",
  "orderable": false,
  "title": "Barcode",
    "render": function(data, type, row, meta){
        return row.qrcode_path;
    }
  },
  {
    className: '',
    "data": "fullname",
    "title": "Name",
  },{
    className: '',
    "data": "municipality",
    "title": "Municipality",
  },{
    className: '',
    "data": "contact_number",
    "title": "Contact number",
  },{
    className: 'width-option-1 text-center',
    "data": "profile_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        newdata += '<button class="btn btn-primary btn-sm font-base mt-1" onclick="" type="button">Print</button>';
        newdata += ' <button class="btn btn-dark btn-sm font-base mt-1" onclick="approve_pending_disapprove(\'0\', \''+row.fullname+'\', \''+row.slug_id+'\')"  type="button">Mark as Pending</button>';
        return newdata;
       }
  }
  ]
  });
  }


  